package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flowers;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private Flowers flowers;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getFlowers() {
		return flowers;
	}

	public void setFlowers() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXHandler handler = new SAXHandler();
		SAXParser parser = null;

		try {
			parser = factory.newSAXParser();
			parser.parse(new File(xmlFileName), handler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		flowers = handler.getFlowers();
	}

	public void save(String fileName) {
		Saver.save(fileName, flowers);
	}

}