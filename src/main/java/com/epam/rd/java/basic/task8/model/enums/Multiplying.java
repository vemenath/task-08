package com.epam.rd.java.basic.task8.model.enums;

public enum Multiplying {
    LEAVES("листья"),
    CUTTINGS("черенки"),
    SEEDS("семена");

    private final String title;

    public static Multiplying getByTitle(String title) {
        for (Multiplying multiplying : Multiplying.values()) {
            if (multiplying.title.equals(title)) {
                return multiplying;
            }
        }
        return null;
    }

    public String getTitle() {
        return title;
    }

    Multiplying(String title) {
        this.title = title;
    }
}
