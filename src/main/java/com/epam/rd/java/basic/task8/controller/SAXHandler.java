package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;
import com.epam.rd.java.basic.task8.model.enums.Multiplying;
import com.epam.rd.java.basic.task8.model.enums.Soil;
import com.epam.rd.java.basic.task8.model.enums.Tags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class SAXHandler extends DefaultHandler {

    private final Flowers flowers = new Flowers();
    private Flower flower;
    private Flower.VisualParameters visualParameters;
    private Flower.GrowingTips growingTips;
    private String stemColour, leafColour, aveLength;
    private int temperature,  watering;
    private boolean lighting;

    public Flowers getFlowers() {
        return flowers;
    }

    @Override
    public void startDocument() throws SAXException {

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        changeTagsState(true, qName, attributes);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        changeTagsState(false, qName, null);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        updateParameter(ch, start, length);
    }

    private void changeTagsState(Boolean state, String qName, Attributes attributes) {
        switch (qName) {
            case Tags.flowers:
                Tags.isFlowers = state;
                break;
            case Tags.flower:
                if (state) {
                    flower = new Flower();
                }
                else {
                    flowers.getFlowers().add(flower);
                }
                Tags.isFlower = state;
                break;
            case Tags.name:
                Tags.isName = state;
                break;
            case Tags.soil:
                Tags.isSoil = state;
                break;
            case Tags.origin:
                Tags.isOrigin = state;
                break;
            case Tags.visualParameters:
                if (state) {
                    visualParameters = new Flower.VisualParameters(stemColour, leafColour, aveLength);
                }
                else {
                    flower.setVisualParameters(visualParameters);
                }
                Tags.isVisualParameters = state;
                break;
            case Tags.stemColour:
                Tags.isStemColour = state;
                break;
            case Tags.leafColour:
                Tags.isLeafColour = state;
                break;
            case Tags.aveLenFlower:
                Tags.isAveLenFlower = state;
                break;
            case Tags.growingTips:
                if (state) {
                    growingTips = new Flower.GrowingTips(temperature, lighting, watering);
                }
                else {
                    flower.setGrowingTips(growingTips);
                }
                Tags.isGrowingTips = state;
                break;
            case Tags.tempreture:
                Tags.isTempreture = state;
                break;
            case Tags.lighting:
                if (state) {
                    lighting = attributes.getValue("lightRequiring").equals("yes");
                    growingTips.setLighting(lighting);
                }
                Tags.isLighting = state;
                break;
            case Tags.watering:
                Tags.isWatering = state;
                break;
            case Tags.multiplying:
                Tags.isMultiplying = state;
                break;
        }
    }

    private void updateParameter(char[] ch, int start, int length) {
        String textContext = new String(ch, start, length);
        if (Tags.isName) {
            flower.setName(textContext);
        }
        else if (Tags.isSoil) {
            flower.setSoil(Soil.getByTitle(textContext));
        }
        else if (Tags.isOrigin) {
            flower.setOrigin(textContext);
        }
        else if (Tags.isVisualParameters) {
            if (Tags.isStemColour) {
                stemColour = textContext;
                visualParameters.setStemColour(stemColour);
            }
            else if (Tags.isLeafColour) {
                leafColour = textContext;
                visualParameters.setLeafColour(leafColour);
            }
            else if (Tags.isAveLenFlower) {
                aveLength = textContext;
                visualParameters.setAveLength(aveLength);
            }
        }
        else if (Tags.isGrowingTips) {
            if (Tags.isTempreture) {
                temperature = Integer.parseInt(textContext);
                growingTips.setTemperature(temperature);
            }
            else if (Tags.isWatering) {
                watering = Integer.parseInt(textContext);
                growingTips.setWatering(watering);
            }
        }
        else if (Tags.isMultiplying) {
            flower.setMultiplying(Multiplying.getByTitle(textContext));
        }
    }
}
