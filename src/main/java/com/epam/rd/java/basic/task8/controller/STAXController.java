package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;
import com.epam.rd.java.basic.task8.model.enums.Multiplying;
import com.epam.rd.java.basic.task8.model.enums.Soil;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private final Flowers flowers = new Flowers();

	public Flowers getFlowers() {
		return flowers;
	}

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void save(String fileName) {
		Saver.save(fileName, flowers);
	}

	public void setFlowers() {
		Flower flower = null;
		Flower.VisualParameters visualParameters = null;
		String stemColour = null, leafColour = null, aveLength = null;
		Flower.GrowingTips growingTips = null;
		int temperature = 0, watering = 0;
		boolean lighting = false;
		XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()) {
				XMLEvent xmlEvent = reader.nextEvent();
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					if (startElement.getName().getLocalPart().equals("flower")) {
						flower = new Flower();
					}
					else if (startElement.getName().getLocalPart().equals("name")) {
						xmlEvent = reader.nextEvent();
						flower.setName(xmlEvent.asCharacters().getData());
					}
					else if (startElement.getName().getLocalPart().equals("soil")) {
						xmlEvent = reader.nextEvent();
						flower.setSoil(Soil.getByTitle(xmlEvent.asCharacters().getData()));
					}
					else if (startElement.getName().getLocalPart().equals("origin")) {
						xmlEvent = reader.nextEvent();
						flower.setOrigin(xmlEvent.asCharacters().getData());
					}
					else if (startElement.getName().getLocalPart().equals("visualParameters")) {
						xmlEvent = reader.nextEvent();
						visualParameters = new Flower.VisualParameters(stemColour, leafColour, aveLength);
						flower.setVisualParameters(visualParameters);
					}
					else if (startElement.getName().getLocalPart().equals("stemColour")) {
						xmlEvent = reader.nextEvent();
						stemColour = xmlEvent.asCharacters().getData();
						visualParameters.setStemColour(stemColour);
					}
					else if (startElement.getName().getLocalPart().equals("leafColour")) {
						xmlEvent = reader.nextEvent();
						leafColour = xmlEvent.asCharacters().getData();
						visualParameters.setLeafColour(leafColour);
					}
					else if (startElement.getName().getLocalPart().equals("aveLenFlower")) {
						xmlEvent = reader.nextEvent();
						aveLength = xmlEvent.asCharacters().getData();
						visualParameters.setAveLength(aveLength);
					}
					else if (startElement.getName().getLocalPart().equals("growingTips")) {
						xmlEvent = reader.nextEvent();
						growingTips = new Flower.GrowingTips(temperature, lighting, watering);
						flower.setGrowingTips(growingTips);
					}
					else if (startElement.getName().getLocalPart().equals("tempreture")) {
						xmlEvent = reader.nextEvent();
						temperature = Integer.parseInt(xmlEvent.asCharacters().getData());
						growingTips.setTemperature(temperature);
					}
					else if (startElement.getName().getLocalPart().equals("lighting")) {
						xmlEvent = reader.nextEvent();
						lighting = startElement.getAttributeByName(new QName("lightRequiring")).getValue().equals("yes");
						growingTips.setLighting(lighting);
					}
					else if (startElement.getName().getLocalPart().equals("watering")) {
						xmlEvent = reader.nextEvent();
						watering = Integer.parseInt(xmlEvent.asCharacters().getData());
						growingTips.setWatering(watering);
					}
					else if (startElement.getName().getLocalPart().equals("multiplying")) {
						xmlEvent = reader.nextEvent();
						flower.setMultiplying(Multiplying.getByTitle(xmlEvent.asCharacters().getData()));
					}
				}

				if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flowers.getFlowers().add(flower);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}