package com.epam.rd.java.basic.task8.model;

import com.epam.rd.java.basic.task8.model.enums.Multiplying;
import com.epam.rd.java.basic.task8.model.enums.Soil;

public class Flower {
    private String name;
    private Soil soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Multiplying multiplying;

    public String getName() {
        return name;
    }

    public Soil getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public Multiplying getMultiplying() {
        return multiplying;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(Soil soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setVisualParameters(String stemColour, String leafColour, String aveLength) {
        visualParameters = new VisualParameters(stemColour, leafColour, aveLength);
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public void setGrowingTips(int temperature, boolean lighting, int watering) {
        growingTips = new GrowingTips(temperature, lighting, watering);
    }

    public void setMultiplying(Multiplying multiplying) {
        this.multiplying = multiplying;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public static class VisualParameters {
        private String stemColour;
        private String leafColour;
        private String aveLength;

        public VisualParameters(String stemColour, String leafColour, String aveLength) {
            this.stemColour = stemColour;
            this.leafColour = leafColour;
            this.aveLength = aveLength;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public void setAveLength(String aveLength) {
            this.aveLength = aveLength;
        }

        public String getStemColour() {
            return stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public String getAveLength() {
            return aveLength;
        }
    }

    public static class GrowingTips {
        private int temperature;
        private boolean lighting;
        private int watering;

        public GrowingTips(int temperature, boolean lighting, int watering) {
            this.temperature = temperature;
            this.lighting = lighting;
            this.watering = watering;
        }

        public void setTemperature(int temperature) {
            this.temperature = temperature;
        }

        public void setLighting(boolean lighting) {
            this.lighting = lighting;
        }

        public void setWatering(int watering) {
            this.watering = watering;
        }

        public int getTemperature() {
            return temperature;
        }

        public boolean isLighting() {
            return lighting;
        }

        public int getWatering() {
            return watering;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: ").append(name);
        sb.append("\nSoil: ").append(soil.getTitle());
        sb.append("\nOrigin: ").append(origin);
        sb.append("\nVisual parameters: ");
        sb.append("\n\tStem colour: ").append(visualParameters.stemColour);
        sb.append("\n\tLeaf colour: ").append(visualParameters.leafColour);
        sb.append("\n\tAverage length: ").append(visualParameters.aveLength);
        sb.append("\nGrowing tips: ");
        sb.append("\n\tLighting: ").append(growingTips.lighting);
        sb.append("\n\tTemperature: ").append(growingTips.temperature);
        sb.append("\n\tWatering: ").append(growingTips.watering);
        sb.append("\nMultiplying: ").append(multiplying.getTitle());

        return sb.toString();
    }
}

