package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.enums.Sorting;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.setFlowers();
		domController.sortFlowers(Sorting.NAME);

		// save
		String outputXmlFile = "output.dom.xml";
		domController.save(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.setFlowers();
		saxController.getFlowers().sort(Sorting.SOIL);
		// save
		outputXmlFile = "output.sax.xml";
		saxController.save(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.setFlowers();
		staxController.getFlowers().sort(Sorting.ORIGIN);

		// save
		outputXmlFile = "output.stax.xml";
		staxController.save(outputXmlFile);
	}

}
