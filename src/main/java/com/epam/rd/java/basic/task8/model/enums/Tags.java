package com.epam.rd.java.basic.task8.model.enums;

public class Tags {
    public static final String flowers = "flowers";
        public static final String flower = "flower";
            public static final String name = "name";
            public static final String soil = "soil";
            public static final String origin = "origin";
            public static final String visualParameters = "visualParameters";
                public static final String stemColour = "stemColour";
                public static final String leafColour = "leafColour";
                public static final String aveLenFlower = "aveLenFlower";
            public static final String growingTips = "growingTips";
                public static final String tempreture = "tempreture";
                public static final String lighting = "lighting";
                public static final String watering = "watering";
            public static final String multiplying = "multiplying";

    public static boolean isFlowers = false;
    public static boolean isFlower = false;
    public static boolean isName = false;
    public static boolean isSoil = false;
    public static boolean isOrigin = false;
    public static boolean isVisualParameters = false;
    public static boolean isStemColour = false;
    public static boolean isLeafColour = false;
    public static boolean isAveLenFlower = false;
    public static boolean isGrowingTips = false;
    public static boolean isTempreture = false;
    public static boolean isLighting = false;
    public static boolean isWatering = false;
    public static boolean isMultiplying = false;
}
