package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;
import com.epam.rd.java.basic.task8.model.enums.Tags;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class Saver {
    public static void save(String fileName, Flowers flowers) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        Document document;
        try {
            documentBuilder = dbf.newDocumentBuilder();
            document = documentBuilder.newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return;
        }

        Element root = document.createElement(Tags.flowers);
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        document.appendChild(root);

        for (Flower flower : flowers) {
            addFlowerElements(document, root, flower);
        }
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(fileName));

            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static void addFlowerElements(Document document, Element root, Flower flower) {
        Element flowerEl = document.createElement(Tags.flower);
        root.appendChild(flowerEl);

        Element name = document.createElement(Tags.name);
        name.setTextContent(flower.getName());
        flowerEl.appendChild(name);

        Element soil = document.createElement(Tags.soil);
        soil.setTextContent(flower.getSoil().getTitle());
        flowerEl.appendChild(soil);

        Element origin = document.createElement(Tags.origin);
        origin.setTextContent(flower.getOrigin());
        flowerEl.appendChild(origin);

        Element visualParameters = document.createElement(Tags.visualParameters);
        flowerEl.appendChild(visualParameters);

        Element stemColour = document.createElement(Tags.stemColour);
        stemColour.setTextContent(flower.getVisualParameters().getStemColour());
        visualParameters.appendChild(stemColour);

        Element leafColour = document.createElement(Tags.leafColour);
        leafColour.setTextContent(flower.getVisualParameters().getLeafColour());
        visualParameters.appendChild(leafColour);

        Element aveLenFlower = document.createElement(Tags.aveLenFlower);
        aveLenFlower.setTextContent(flower.getVisualParameters().getAveLength());
        aveLenFlower.setAttribute("measure", "cm");
        visualParameters.appendChild(aveLenFlower);

        Element growingTips = document.createElement(Tags.growingTips);
        flowerEl.appendChild(growingTips);

        Element tempreture = document.createElement(Tags.tempreture);
        tempreture.setTextContent(Integer.toString(flower.getGrowingTips().getTemperature()));
        tempreture.setAttribute("measure", "celcius");
        growingTips.appendChild(tempreture);

        Element lighting = document.createElement(Tags.lighting);
        lighting.setAttribute("lightRequiring", flower.getGrowingTips().isLighting() ? "yes" : "no");
        growingTips.appendChild(lighting);

        Element watering = document.createElement(Tags.watering);
        watering.setTextContent(Integer.toString(flower.getGrowingTips().getWatering()));
        watering.setAttribute("measure", "mlPerWeek");
        growingTips.appendChild(watering);

        Element multiplying = document.createElement(Tags.multiplying);
        multiplying.setTextContent(flower.getMultiplying().getTitle());
        flowerEl.appendChild(multiplying);
    }
}
