package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.Flowers;
import com.epam.rd.java.basic.task8.model.enums.Multiplying;
import com.epam.rd.java.basic.task8.model.enums.Soil;
import com.epam.rd.java.basic.task8.model.enums.Sorting;
import com.epam.rd.java.basic.task8.model.enums.Tags;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private final String xmlFileName;
	private final Flowers flowers = new Flowers();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void setFlowers() {
		File file = new File(xmlFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document doc;
		try {
			doc = dbf.newDocumentBuilder().parse(file);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		NodeList flowersList = doc.getElementsByTagName("flower");

		for (int i = 0; i < flowersList.getLength(); i++) {
			Node root = flowersList.item(i);
			if (root.getNodeType() != Node.ELEMENT_NODE) {
				return;
			}
			flowers.getFlowers().add(setFlower(root));
		}
	}

	public void sortFlowers(Sorting sorting) {
		flowers.sort(sorting);
	}

	public void save(String fileName) {
		Saver.save(fileName, flowers);
	}

	private Flower setFlower(Node node) {
		Element element = (Element) node;
		Flower flower = new Flower();

		flower.setName(element.getElementsByTagName(Tags.name).item(0).getTextContent());
		flower.setSoil(Soil.getByTitle(element.getElementsByTagName(Tags.soil).item(0).getTextContent()));
		flower.setOrigin(element.getElementsByTagName(Tags.origin).item(0).getTextContent());

		String stemColour = element.getElementsByTagName(Tags.stemColour).item(0).getTextContent();
		String leafColour = element.getElementsByTagName(Tags.leafColour).item(0).getTextContent();
		String aveLenFlower = element.getElementsByTagName(Tags.aveLenFlower).item(0).getTextContent();
		flower.setVisualParameters(stemColour, leafColour, aveLenFlower);

		int temperature = Integer.parseInt(element.getElementsByTagName(Tags.tempreture).item(0).getTextContent());
		Element lightingElement = (Element) element.getElementsByTagName(Tags.lighting).item(0);
		boolean lighting = lightingElement.getAttribute("lightRequiring").equals("yes");
		int watering = Integer.parseInt(element.getElementsByTagName(Tags.watering).item(0).getTextContent());
		flower.setGrowingTips(temperature, lighting, watering);

		flower.setMultiplying(Multiplying.getByTitle(element.getElementsByTagName(Tags.multiplying).item(0).getTextContent()));

		return flower;
	}

}
