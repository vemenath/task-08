package com.epam.rd.java.basic.task8.model;

import com.epam.rd.java.basic.task8.model.enums.Sorting;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class Flowers implements Iterable<Flower> {
    private final List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void sort(Sorting sorting) {
        switch (sorting) {
            case NAME:
                flowers.sort(Comparator.comparing(Flower::getName));
                break;
            case SOIL:
                flowers.sort(Comparator.comparing(Flower::getSoil));
                break;
            case ORIGIN:
                flowers.sort(Comparator.comparing(Flower::getOrigin));
                break;
        }
    }

    @Override
    public Iterator<Flower> iterator() {
        return flowers.iterator();
    }

    @Override
    public void forEach(Consumer action) {
        flowers.forEach(action);
    }
}
