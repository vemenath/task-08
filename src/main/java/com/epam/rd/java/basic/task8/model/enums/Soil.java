package com.epam.rd.java.basic.task8.model.enums;

public enum Soil {
    PODZOLIC("подзолистая"),
    UNPAVED("грунтовая"),
    SOD_PODZOLIC("дерново-подзолистая");

    private final String title;

    public static Soil getByTitle(String title) {
        for (Soil soil : Soil.values()) {
            if (soil.title.equals(title)) {
                return soil;
            }
        }
        return null;
    }

    public String getTitle() {
        return title;
    }

    Soil(String title) {
        this.title = title;
    }
}
