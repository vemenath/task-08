package com.epam.rd.java.basic.task8.model.enums;

public enum Sorting {
    NAME, SOIL, ORIGIN
}
